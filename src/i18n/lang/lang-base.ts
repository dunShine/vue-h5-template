export type langType = {
  title: string;
  tabbar: {
    home: string;
    payment: string;
    navigation: string;
    mine: string;
  };
  language: {
    en: string;
    zh: string;
  };
  introduction: string;
  home: {
    support: string;
    cssMultiLanguage: string;
  };
  list: {
    details: string;
  };
};
